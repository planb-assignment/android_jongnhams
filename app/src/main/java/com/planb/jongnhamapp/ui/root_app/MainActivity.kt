package com.planb.jongnhamapp.ui.root_app

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.ui.coupon.CouponFragment
import com.planb.jongnhamapp.ui.favorite.FavoriteFragment
import com.planb.jongnhamapp.ui.home.HomeFragment
import com.planb.jongnhamapp.ui.mapview.MapsFragment.Companion.PERMISSION_REQUEST_ACCESS_FINE_LOCATION
import com.planb.jongnhamapp.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val homeFragment = HomeFragment()
        val couponFragment = CouponFragment()
        val favoriteFragment = FavoriteFragment()
        val profileFragment = ProfileFragment()

        setCurrentFragment(homeFragment)
        requestLocationPermission();

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home->setCurrentFragment(homeFragment)
                R.id.navigation_coupon->setCurrentFragment(couponFragment)
                R.id.navigation_favorite->setCurrentFragment(favoriteFragment)
                R.id.navigation_profile->setCurrentFragment(profileFragment)

            }
            true
        }
    }
    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment,fragment)
            commit()
        }

    fun requestLocationPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }
}

