package com.planb.jongnhamapp.ui.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.model.SearchContentModel
import kotlinx.android.synthetic.main.fragment_search.*


class SearchFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_search, container, false)
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvSearch.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvSearch.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        rvSearch.adapter = context?.let { SearchContentAdapter(contentList = list, context = it) }
    }

    var list  = arrayListOf<SearchContentModel>(
            SearchContentModel(
                    img = R.drawable.aws,
                    title = "Café Amazon - In PTT",
                    rateAmount = 20,
                    star = 5,
                    distance = 0.8,
                    location = "Phnom Penh",
                    views = 400,
                    category = "Coffee",
                    locationCoords = LatLng(11.5480498, 104.8807732)
            ),
            SearchContentModel(
                    img = R.drawable.arabitia,
                    title = "Arabitia Coffee",
                    rateAmount = 20,
                    star = 5,
                    distance = 0.8,
                    location = "Kampong Cham",
                    views = 400,
                    category = "Coffee",
                    locationCoords = LatLng(11.5480824,104.8936444)
            ), SearchContentModel(
            img = R.drawable.truecoffee,
            title = "True Coffee ",
            rateAmount= 20,
            star= 4,
            distance= 0.8,
            location= "Phnom Penh",
            views= 200,
            category= "Coffee",
            locationCoords= LatLng(11.5470762,104.8997676)
    ),
            SearchContentModel(
                    img = R.drawable.coffee1979,
                    title = "Coffee 1979",
                    rateAmount = 20,
                    star = 3,
                    distance = 0.8,
                    location = "Phnom Penh",
                    views = 100,
                    category = "Coffee",
                    locationCoords = LatLng(11.5545482,104.9066904)
            ),
            SearchContentModel(
                    img = R.drawable.tpr,
                    title = "TPR Coffee",
                    rateAmount = 60,
                    star = 5,
                    distance = 0.8,
                    location = "Phnom Penh City",
                    views = 50,
                    category = "Coffee",
                    locationCoords = LatLng(11.5408112, 104.8805845)
            ),
            SearchContentModel(
                    img = R.drawable.tpr,
                    title =  "Premium Coffee",
                    rateAmount =  90,
                    star = 5,
                    distance =  0.5,
                    location = "Teuk Tla・Rupp",
                    views = 50,
                    category = "Coffee",
                    locationCoords = LatLng(11.5854736, 104.8814846)
            )
    )

}