package com.planb.jongnhamapp.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.model.SearchContentModel

class SearchContentAdapter(var contentList: ArrayList<SearchContentModel>, var context:Context) :
    RecyclerView.Adapter<SearchContentAdapter.MySearchViewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MySearchViewHolder {
        return SearchContentAdapter.MySearchViewHolder(
            LayoutInflater.from(context).inflate(R.layout.search_content_item, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return  contentList.size;
    }

    override fun onBindViewHolder(holder: MySearchViewHolder, position: Int) {
        val data = contentList[position]
        Glide
            .with(context)
            .load(data.img)
            .centerCrop()
            .placeholder(R.drawable.banner1)
            .into(holder.imageView)
        holder.title.text = data.title
        holder.category.text = data.category
        holder.distance.text = "${data.distance.toString()} km"
        holder.location.text = data.location
        holder.rating.text = "${data.rateAmount} Ratings"
        holder.ratingAmount.text = "${data.star}.0"
        holder.review.text = "${data.rateAmount} Reviews"
        holder.ratingStar.rating = data.rateAmount.toFloat()
        holder.view.text = "${data.views} Views"

    }
    class MySearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView = itemView.findViewById<ImageView>(R.id.imageView)
        var title = itemView.findViewById<TextView>(R.id.tvTitle)
        var distance = itemView.findViewById<TextView>(R.id.bDistance)
        var ratingStar = itemView.findViewById<RatingBar>(R.id.ratingBar)
        var ratingAmount = itemView.findViewById<TextView>(R.id.tvAmountRate)
        var view = itemView.findViewById<TextView>(R.id.tvView)
        var rating = itemView.findViewById<TextView>(R.id.tvTotalRating)
        var review = itemView.findViewById<TextView>(R.id.tvTotalReview)
        var category = itemView.findViewById<TextView>(R.id.tvCategory)
        var location = itemView.findViewById<TextView>(R.id.tvLocation)

    }
}