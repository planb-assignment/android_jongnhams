package com.planb.jongnhamapp.ui.mapview

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.model.SearchContentModel
import kotlinx.android.synthetic.main.activity_maps.rvMapContent
import kotlinx.android.synthetic.main.fragment_maps.*


class MapsFragment : Fragment() {
    companion object {
         const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
    }
    lateinit var mMap: GoogleMap
     var  latLng = LatLng(11.5793306,104.7500973)
    private val callback = OnMapReadyCallback { googleMap ->
        mMap = googleMap

        val locationManager = context?.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager?

        val locationListener = LocationListener {
            latLng = LatLng(it.latitude, it.longitude)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
            mMap.uiSettings.isMyLocationButtonEnabled = true
            mMap.addMarker(MarkerOptions().title("Current Location").position(latLng).icon(context?.let { it1 -> bitmapDescriptorFromVector(it1, R.drawable.ic_baseline_location_on_24) }))
            mMap.uiSettings.isMapToolbarEnabled = true
            for (data in list){
                createMarker(
                        latitude = data.locationCoords!!.latitude,
                        longitude = data.locationCoords!!.longitude,
                        title = data.title
                )
            }

        }
        try {
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
        } catch (ex: SecurityException) {
            Toast.makeText(context, "Not allow permission", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) }
            != PackageManager.PERMISSION_GRANTED) {
            activity?.let {
                ActivityCompat.requestPermissions(
                        it,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSION_REQUEST_ACCESS_FINE_LOCATION
                )
            }
        }
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        val contentAdapter = context?.let {
            ContentMapAdapter(list = list, context = it){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(it.locationCoords, 16f))
            }
        }

        rvMapContent.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvMapContent.adapter =  contentAdapter
        rvMapContent.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager =
                        LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
                val totalItemCount = layoutManager.itemCount
                val lastVisible = layoutManager.findLastVisibleItemPosition()
                val endHasBeenReached = lastVisible + 5 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached) {
                    val data = list.get(lastVisible)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(data.locationCoords))


                }
            }
        })

        imgMyLocation.setOnClickListener {
            if(latLng.latitude != 0.0) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
            }
        }
    }

    protected fun createMarker(
            latitude: Double,
            longitude: Double,
            title: String?
    ): Marker {
        return mMap.addMarker(
                MarkerOptions()
                        .position(LatLng(latitude, longitude))
                        .anchor(0.5f, 0.5f)
                        .snippet(title)
                        .title(title)
                        .icon(context?.let { bitmapDescriptorFromVector(it, R.drawable.ic_baseline_person_pin_circle_24) }


        ))
    }
    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val background = ContextCompat.getDrawable(context, R.drawable.ic_baseline_person_pin_circle_24)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(60, 40, vectorDrawable.intrinsicWidth + 80, vectorDrawable.intrinsicHeight + 40)
        val bitmap = Bitmap.createBitmap(background.intrinsicWidth, background.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    var list  = arrayListOf<SearchContentModel>(
            SearchContentModel(
                    img = R.drawable.aws,
                    title = "Café Amazon - In PTT",
                    rateAmount = 20,
                    star = 5,
                    distance = 0.8,
                    location = "Phnom Penh",
                    views = 400,
                    category = "Coffee",
                    locationCoords = LatLng(11.5480498, 104.8807732)
            ),
            SearchContentModel(
                    img = R.drawable.arabitia,
                    title = "Arabitia Coffee",
                    rateAmount = 20,
                    star = 5,
                    distance = 0.8,
                    location = "Kampong Cham",
                    views = 400,
                    category = "Coffee",
                    locationCoords = LatLng(11.5480824, 104.8936444)
            ), SearchContentModel(
            img = R.drawable.truecoffee,
            title = "True Coffee ",
            rateAmount = 20,
            star = 4,
            distance = 0.8,
            location = "Phnom Penh",
            views = 200,
            category = "Coffee",
            locationCoords = LatLng(11.5470762, 104.8997676)
    ),
            SearchContentModel(
                    img = R.drawable.coffee1979,
                    title = "Coffee 1979",
                    rateAmount = 20,
                    star = 3,
                    distance = 0.8,
                    location = "Phnom Penh",
                    views = 100,
                    category = "Coffee",
                    locationCoords = LatLng(11.5545482, 104.9066904)
            ),
            SearchContentModel(
                    img = R.drawable.tpr,
                    title = "TPR Coffee",
                    rateAmount = 60,
                    star = 5,
                    distance = 0.8,
                    location = "Phnom Penh City",
                    views = 50,
                    category = "Coffee",
                    locationCoords = LatLng(11.5408112, 104.8805845)
            ),
            SearchContentModel(
                    img = R.drawable.tpr,
                    title = "Premium Coffee",
                    rateAmount = 90,
                    star = 5,
                    distance = 0.5,
                    location = "Teuk Tla・Rupp",
                    views = 50,
                    category = "Coffee",
                    locationCoords = LatLng(11.5854736, 104.8814846)
            )
    )

}