package com.planb.jongnhamapp.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.imageview.ShapeableImageView
import com.planb.jongnhamapp.R

class CouponAdapter(val photos: ArrayList<Int>, var context:Context) : RecyclerView.Adapter<CouponAdapter.MyViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.image_carousel_item, parent, false))

    }

    override fun getItemCount(): Int {
        return  photos.size;
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Glide
            .with(context)
            .load(photos[position])
            .centerCrop()
            .placeholder(R.drawable.banner1)
            .into(holder.imageView)

    }

    class  MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView =
            itemView.findViewById<ShapeableImageView>(R.id.imageView)
    }
}