package com.planb.jongnhamapp.ui.mapview

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.model.SearchContentModel
import kotlinx.android.synthetic.main.content_map_item.view.*

class ContentMapAdapter(var list:ArrayList<SearchContentModel>, val  context: Context, val clickListener: (SearchContentModel) -> Unit) :
    RecyclerView.Adapter<ContentMapAdapter.ContentViewHolder>()  {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        return ContentMapAdapter.ContentViewHolder(
            LayoutInflater.from(context).inflate(R.layout.content_map_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return  list.size;
    }

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        val data = list[position]
        Glide
            .with(context)
            .load(data.img)
            .centerCrop()
            .placeholder(R.drawable.banner1)
            .into(holder.img)
        holder.distance.text = "${data.distance.toString()} km"
        holder.location.text = data.location
        holder.title.text = data.title

        holder?.itemView?.setOnClickListener { clickListener(data) }
        holder.itemView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, m: MotionEvent): Boolean {
                clickListener(data)
                return true
            }
        })

    }


    class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title = itemView.tvTitle
        var location = itemView.tvLocation
        var img = itemView.img
        var distance = itemView.bDistance

    }


}