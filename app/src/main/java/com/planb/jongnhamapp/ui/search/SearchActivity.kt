package com.planb.jongnhamapp.ui.search

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.planb.jongnhamapp.R
import com.planb.jongnhamapp.ui.mapview.MapsFragment
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        supportFragmentManager.beginTransaction().
        add(R.id.fragmentContainer, SearchFragment(),"searchFragment").commit()
    }


    fun onBackHome(view: View) {
        onBackPressed()
    }

    fun onGoogleMap(view: View) {
        imgMap.visibility = View.VISIBLE
        imgSearch.visibility = View.GONE
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, MapsFragment(), "mapsFragment").commit()
    }

    fun onSearch(view: View) {
        imgMap.visibility = View.GONE
        imgSearch.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, SearchFragment(), "searchFragment").commit()
    }
}