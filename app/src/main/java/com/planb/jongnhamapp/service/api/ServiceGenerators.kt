package com.planb.jongnhamapp.service.api

import com.planb.jongnhamapp.helper.Constant.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {
        private val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        fun <S> createService(service: Class<S>): S {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .addHeader("Authorization", "API_KEY")
                    .header("Accept", "application/json")

                val request = requestBuilder.build()
                chain.proceed(request)
            }

            val retrofit = builder
                .client(httpClient.build())
                .build()
            return retrofit.create(service)
        }
    }


