package com.planb.jongnhamapp.helper;

import android.content.Context;

public class SharepreferenceHelper {

    Context context;

    public SharepreferenceHelper(Context context) {
        this.context = context;
    }

    public void saveStringValue(String key, String value){
        context.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE)
                .edit().putString(key, value).apply();
    }

    public String getStringValue(String key){
        return context.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE)
                .getString(key, null);
    }

    public void  removeStringValue(String key){
        context.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE)
        .edit().remove(key).apply();
    }



}
