package com.planb.jongnhamapp.model

import com.google.android.gms.maps.model.LatLng

class SearchContentModel {
    var img : Int = 0
    var rateAmount : Int = 0
    var title : String = ""
    var category: String = ""
    var location : String = ""
    var distance : Double = 0.0
    var views : Int = 0
    var star : Int = 0;
    var locationCoords : LatLng? = null

    constructor(
        img: Int,
        rateAmount: Int,
        title: String,
        category: String,
        location: String,
        distance: Double,
        views: Int,
        star: Int,
        locationCoords: LatLng?
    ) {
        this.img = img
        this.rateAmount = rateAmount
        this.title = title
        this.category = category
        this.location = location
        this.distance = distance
        this.views = views
        this.star = star
        this.locationCoords = locationCoords
    }
}